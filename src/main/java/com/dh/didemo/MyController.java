package com.dh.didemo;

import org.springframework.stereotype.Controller;

@Controller
public class MyController {
    public String hello(){
        String greeting = "hello string";
        System.out.println(greeting);
        return greeting;
    }
}
